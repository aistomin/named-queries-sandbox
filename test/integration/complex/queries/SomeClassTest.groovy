package complex.queries

/**
 * User: aistomin Date: 13.8.14 Time: 23.55
 */
class SomeClassTest extends GroovyTestCase {

    void testNamedQueries() {
        def entity = new SomeClass(name: "Any Name", age: 19).save(flush: true)

        def found = SomeClass.searchByNameAndAge("Any Name", 19)

        assertEquals(1, found.size())
        assertEquals(entity.id, found.first().id)

        def notFound = SomeClass.searchByNameAndAge("Other name", 19)
        assertEquals(0, notFound.size())

        found = SomeClass.searchByNameOrAge("Any Name", 20).list()
        assertEquals(1, found.size())
        assertEquals(entity.id, found.first().id)

        notFound = SomeClass.searchByNameOrAge("Other name", 14).list()
        assertEquals(0, notFound.size())
    }
}
