### What is this repository for? ###

Despite of good documentation, by some reasons, I had problems with named queries in Grails. I decided to publish project where I was playing with such queries. 
Maybe it helps some newbies(like me) to start with named queries more quickly than I did.
In this repository you will find the examples of:

1. How to use one named query inside the other.
2. How to use named queries in chain(call them via ".")
