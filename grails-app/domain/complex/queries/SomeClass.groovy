package complex.queries

/**
 * User: aistomin Date: 13.8.14 Time: 23.54
 *
 * Some domain class.
 */
class SomeClass {

    String name
    Integer age

    static constraints = {
        name unique: true, nullable: false
        age  nullable: false
    }

    /**
     * Example of using chained named queries.
     */
    static List<SomeClass> searchByNameAndAge (String name, Integer age) {
        searchByName(name).searchByAge(age).list()
    }

    static namedQueries = {
        searchByName { String name ->
            eq('name', name)
        }

        searchByAge { Integer age ->
            eq('age', age)
        }

        // example of using one named query inside another.
        searchByNameOrAge { String name, Integer age ->
            or {
                searchByName(name)
                searchByAge(age)
            }
        }
    }
}
